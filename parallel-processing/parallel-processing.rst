.. _sec-parallel-processing:

=====================
 Parallel processing
=====================

Motivation and plan
===================

Parallel processing is key to high performance computing, but it is
important for every hacker to know how programs do this kind of
processing.

Concepts
========

.. glossary::

   CPU
      A physical CPU chip.

   core
      One of several logical CPUs inside a single physical CPU.

   process
      An separate flow of execution with its own memory and
      instructions.  Separate processes appear to execute at the same
      time, and sometimes they do (when you have multiple cores).

   thread
      A light-weight process which shares data with other threads in
      the same process.

   multiprocessing
      ...

   multithreading
      ...

   race condition
      With multithreading and multiprocessing you could have one
      execution threads trying to access memory while another is
      modifying it.

   synchronization
      The collection of techniques that help resolve race conditions.

   :index:`mutex`
      A "mutual exclusion" object which allows programs to decide who
      is currently using a resource.


Python multithreading library
=============================

.. _listing-simplest-thread:

.. literalinclude:: simplest-thread.py
   :language: python
   :caption: Simplest multithreading program.

.. _listing-thread-with-arg:

.. literalinclude:: thread-with-arg.py
   :language: python
   :caption: Multithreading program with a bit more.

.. _listing-thread-with-some-work:

.. literalinclude:: thread-with-some-work.py
   :language: python
   :caption: Multithreading program with actual work.
