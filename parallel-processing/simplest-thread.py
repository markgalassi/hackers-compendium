#! /usr/bin/env python3

import threading


def main():
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker)
        threads.append(t)
        t.start()

def worker():
    """thread worker function"""
    print('Worker')
    return

main()
