#! /usr/bin/env python3

import threading
import time
import random

def main():
    service_thread = threading.Thread(name='my_service', target=my_service)
    worker_list = []
    print('creating the threads')
    for i in range(5):
        w = threading.Thread(name='worker_' + str(i), target=worker)
        worker_list.append(w)

    print('starting the service and worker threads')
    service_thread.start()
    for i in range(5):
        worker_list[i].start()

    for i in range(5):
        worker_list[i].join()
    service_thread.join()
    print('** main program is at an end **')

def worker():
    """sleep a random amount of time and exit"""
    myname = threading.currentThread().getName()
    sleep_time = random.randint(0, 10)
    print(myname, 'Starting')
    time.sleep(sleep_time)
    print(myname, 'Exiting after', sleep_time, 'seconds')

def my_service():
    """long running service"""
    myname = threading.currentThread().getName()
    sleep_time = 20
    print(myname, 'Starting')
    time.sleep(sleep_time)
    print(myname, 'Exiting after', sleep_time, 'seconds')

main()
