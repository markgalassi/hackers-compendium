============================
 C++ Thoughts and resources
============================

Useful blogs and resources
==========================

https://www.codingame.com/playgrounds/5659/c17-filesystem


Binary compatibility issues
===========================

https://community.kde.org/Policies/Binary_Compatibility_Issues_With_C%2B%2B


Exception handling
==================

In

https://blog.quasardb.net/2015/02/04/boost-filesystem-and-error-handling

you have the phrase "Exceptions in C++ is a hot topic but our design
decision is to use exceptions as little as possible as they often
obfuscate important error paths. We have many critical moments in the
software (for example when sending a reply on the network or writing
to the disk) where all error paths must be extremely clear in the code
to make sure we don’t get in a situation where we incorrectly recover
from an error."


