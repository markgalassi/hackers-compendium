=================================
 Motivation and plan of the work
=================================

My motivation is to collect a variety techniques that a GNU or UNIX
hacker might find intriguing and which would help in her development.

Part of the approach is to pick "just the right" collection of topics,
then to explain some of the concepts.

But maybe more important is to then quickly dive in to a set of
bite-sized mini projects, showing little tasks that come up in
someone's day on a computer, and how a hacker would carry out those
tasks efficiently.

But there is always a slant: in each topic I plan to include some of
the techniques I use to look at a GNU/Linux or UNIX issue and which I
don't see other people using much.  It might end up being a bunch of
tips on how I do things.

What is a hacker?
=================

The term *hacker* is so often mis-used that for most people it is
important to tell you how I think of the word: a hacker is a person
who knows her computer so intimately that she can make it do anything
for her.

This often involves some ease in writing software, a clear picture of
how the hardware and operating system work, as well as in-depth
knowledge of software tools and how to put them together.
