# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = HackersCompendium
SOURCEDIR     = .
BUILDDIR      = _build

dist-galassi-org: html
	rsync --progress --partial --delete -avz $(BUILDDIR)/html/ agora.rdrop.com:web-galassi.org/mark/hackers-compendium
	if [ -d ~/web-galassi.org ]; then \
		echo LOCAL; \
		rsync --delete -avz $(BUILDDIR)/html/ ~/web-galassi.org/mark/hackers-compendium; \
	else \
		echo REMOTE; \
		rsync --delete -avz $(BUILDDIR)/html/ mark.galassi.org:web-galassi.org/mark/hackers-compendium; \
	fi


# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
