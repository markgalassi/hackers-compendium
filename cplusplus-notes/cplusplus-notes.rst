===========
 C++ notes
===========

This is not yet really written, but here's the idea:

Programming in C++ comes up a lot.  Most C++ programmers use C++
withought understanding the grave consequences that this can have on
their project.

So it is important to (a) understand C++ conceptually and in the broad
picture of programming, (b) understand when it's possible to avoid
it, (c) learn best practices to avoid some of the problems.

How C++ fits into the landscape of programming
==============================================

How to avoid using C++
======================

Best practices in C++
=====================

Source indenters
================

The classic C indent only does a half-hearted attempt.

artistic indent: aindent

Examples of specific questions
==============================

To see how some people think through some aspects of C++ here are some
discussions.

https://stackoverflow.com/questions/14741144/stdstringstream-vs-stdstring-for-concatenating-many-strings

Resources
=========

Good practices for exception handling:

https://stackoverflow.com/questions/134569/c-exception-throwing-stdstring
which points to
https://www.boost.org/community/error_handling.html
