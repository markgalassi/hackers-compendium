.. Hacker's Compendium documentation master file, created by
   sphinx-quickstart on Sat Sep 16 07:52:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hacker's Compendium
===================

   :Date: |today|
   :Author: **Mark Galassi**

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   intro.rst
   the-teachings-of-jake/the-teachings-of-jake.rst
   dos-and-donts/dos-and-donts.rst
   your-own-rabbit-hole/your-own-rabbit-hole.rst
   favorite-shell-techniques/favorite-shell-techniques.rst
   games-and-diversions/games-and-diversions.rst
   graphical-environment/graphical-environment.rst
   compiling-and-running-basics/compiling-and-running-basics.rst
   probing-memory/probing-memory.rst
   pointers/pointers.rst
   cxx-thoughts-and-resources/cxx-thoughts-and-resources.rst
   parallel-processing/parallel-processing.rst
   fun-instructive-programs/fun-instructive-programs.rst
   sharing-on-the-net/sharing-on-the-net.rst
   packaging-debian/packaging-debian.rst
   cplusplus-notes/cplusplus-notes.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
