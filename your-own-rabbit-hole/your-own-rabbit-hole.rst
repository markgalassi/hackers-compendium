.. _chap-your-own-rabbit-hole:

======================
 Your own rabbit hole
======================

The expression "going down a rabbit hole" is sometimes used to refer
to excluding the rest of the world.  From the urban dictionary:

.. epigraph::

   `Down the rabbit hole`

   To get extremely and obsessively involved in something to the
   exclusion of normal friends and family.

   "I haven't seen Josh since he started playing that Ingress"

   "Yep, down the rabbit hole."

   -- urbandictionary https://www.urbandictionary.com/define.php?term=Down%20the%20rabbit%20hole

One personal experience
=======================

End of graduate school, summer 1992, I'm spending the night working in
a dingy office in the Los Alamos theoretical division building.  I
feel the glare from my monitor, and I remember with pleasure the
TVI-950 terminal which had a dark background and amber characters - it
had always felt so soothing.

The default background and foreground colors on xterm windows at the
time caused a lot of glare: lots of white light hitting my eyes.  I
wanted to explore what backgrounds might be soothing.

If you encode the red, green, and blue channel values in single bytes
(0 to 255), then you have approximately 16 million possible colors.
That's too much to try out, even if you're willing to stare at the
screen all night.

If you look at the file ``/etc/X11/rgb.txt`` (back then I think it was
just ``/etc/rgb.txt``) you will see a list of common colors, with
English names.  It looks like this:

::

   255 250 250		snow
   248 248 255		ghost white
   248 248 255		GhostWhite
   245 245 245		white smoke
   245 245 245		WhiteSmoke
   220 220 220		gainsboro
   255 250 240		floral white
   255 250 240		FloralWhite
   253 245 230		old lace
   253 245 230		OldLace
   250 240 230		linen
   250 235 215		antique white
   250 235 215		AntiqueWhite
   

I don't recognize most of them, but it doesn't matter.  I wrote a
script that pulled out all the color names from that file, and ran

::

   xterm -bg BG_COLOR -fg white

and allowed me to look at it and see if I wanted it to be my
background.  After staring at them all night I finally settled on a
dark olive green, a brownish red, and a couple of other dark
backgrounds.

The main point of this is not the choice of backgrounds, but that I
spent a night writing a script to make it convenient to evaluate them
all.  It might not have been 

At other times I have worked on perfecting email filters, coming up
with a tailored approach to backups, and various 


Customizing your desktop for power and beauty
=============================================

Discuss concept issues: *window system*, *window*, *window manager*,
*desktop environment* (and its components).

There are many approaches to running your desktop.  At this time
(2021-01) I use the compiz window manager with 8 virtual desktops, a
three dimensional rotating cube approach, and various ways of looking
for windows among the hundreds that I use.  I add to that the Mate
desktop environment, but I do not make much use of its features.

Some still use a window manager with no desktop environment, and there
is much to be said for that as well.

Some window managers get quite radical, discouraging individual
placement of windows.  These are the tiling window managers like
*awesome* and others described at

https://en.wikipedia.org/wiki/Tiling_window_manager

There is a never-ending list of desktop environments, and it might be
worth experimenting with many of them.  This can be done using "nested
X window system" approaches.  The major ones are GNOME, KDE, xfce,
Mate, Cinnamon, enlightenment.
