.. _chap-dos-and-donts:

================
 Dos and don'ts
================

[status: not yet written]

DO: Know your lineage.  For example, I come from a tradition, and if
you like to learn from what I am telling you here then you are part of
that tradition.  The tradition comes largely from the hackers at MIT
who had early access to computers and created the environment of
technical excellence that my mentors taught me.  [citation to Levy's
book "Hackers"]

DO: Only run free software.

DO: Have a space on the web where you can put things for others to
download (see what github and bitbucket offer as literal web sites).

DO: Have a space on the web where people can upload things for you.

DO: Be aware of many programs rather intimately.  Curate your own
collection.  It should overlap with others (everyone will know
``grep``, for example), but it should also reflect what you have found
when you formed your list (for example, only some people might be
ready to use the options for ``ffmpeg`` without looking them up).

DO: Make sure that you occasionally go down a rabbit hole.  Every year or
so spend a couple of days chasing some goal in customizing your
system, or writing a program, that seems to have nothing to do with
your job.

DON'T: Don't go down those rabbit holes too often!

DON'T: Never, ever, ever, ever put spaces and other fancy characters
into filenames.  File names should consist of upper case letters,
lower case letters, underscores, hyphens and periods.  Nothing else.

Endnote:

Vis-a-vis spelling of "Dos and don'ts", see:
https://www.quickanddirtytips.com/education/grammar/dos-and-donts
