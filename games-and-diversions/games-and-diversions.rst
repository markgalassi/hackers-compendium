.. _chap-games-and-diversions:

======================
 Games and diversions
======================

Motivation
==========

You should know about games diversions on your system, even if you
don't use them too much.

Simple diversions and command-line fu
=====================================

Try the folloing commands:

::

   fortune
   cowsay dude
   fortune | cowsay
   ddate
   lolcat
   pipe anything through lolcat, such as: fortune | lolcat
   sl # (what happens when you mistype ls?)
   rev
   fortune | rev
   cmatrix
   moo
   factor 27000
   espeak "Hello Linux, where are the penguins"
   aafire -driver curses

A typing game: the original adventure!

:: 

   adventure

Some math stuff:

::

   factor 12103  # factoring numbers? can we use this to search for Mersenne primes?
   factor `echo "2^7-1" | bc` ; factor `echo "2^11-1" | bc` ; factor `echo "2^13-1" | bc`
   pi 50



Packages that offer many games
==============================

gtkboard
   ::

      sudo apt install gtkboard
      gtkboard &
kgames
   Keth Packard's collection of simple games with the MIT Athena
   widgets from the 1980s.
   ::

      sudo apt install kgames
      dpkg -L kgames | grep /usr/games
bsdgames
   The games from old Berkeley UNIX:
   ::

      sudo apt install bsdgames
      dpkg -L bsdgames | grep /usr/games
bsdgames-nonfree
   ::

      sudo apt install bsdgames-nonfree
      dpkg -L bsdgames-nonfree | grep /usr/games
      # you see rogue, so try:
      /usr/games/rogue

Games in emacs
==============

tetris in emacs
   In emacs: ``M-x tetris``
doctor (a version of Eliza) in emacs
   In emacs: ``M-x doctor``


Character graphics with curses
==============================

roguelike games
   ``sudo apt install games-rogue``
nethack
   ``sudo apt install nethack``
nethack
   ``sudo apt install angband``
nethack
   ``sudo apt install moria``
omega
   ``sudo apt install omega-rpg``
larn
   https://larn.org/ (unfortunately not a FOSS license)  

Graphical games
===============

Retro
-----

pacman
   ``sudo apt install pacman``
Nintendo emulator
    ::

       sudo apt install zsnes

    Then you have to find the ROM images.  There might be licensing
    issues.  This should be seen as a curiosity.


Modern graphics
---------------

doomsday
   ``sudo apt install doomsday``
quake
   ``sudo apt install quake``
wesnoth
   sudo apt install wesnoth
openarena
   sudo apt install openarena
0ad
   sudo apt install 0ad
supertuxkart
   sudo apt install tuxcart
extremetuxracer
   sudo apt install extremetuxracer


Other resources
===============

https://itsfoss.com/play-retro-games-linux/

https://osgameclones.com/
