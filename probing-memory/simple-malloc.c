#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

/* compile with: */
/* gcc simple-malloc.c -o simple-malloc */

int main(int argc, char *argv[])
{
  if (argc != 3) {
    printf("dude, wrong args\n");
    printf("usage: %s size_MB duration_sec\n", argv[0]);
    exit(1);
  }
  unsigned long size = atol(argv[1]);     /* in MB */
  int duration = atoi(argv[2]); /* in sec */
  printf("requesting %ld bytes for %d seconds\n", size*1024*1024, duration);
  char *ptr = malloc(size * 1024 * 1024);
  // note that we are not checking the result of the allocation
  memset(ptr, 'x', size*1024*1024);
  ptr[104] = 'y';
  ptr[107] = '\0';
  printf("%s\n", ptr);
  sleep(duration);
  free(ptr);                    /* we're done with it */
}
