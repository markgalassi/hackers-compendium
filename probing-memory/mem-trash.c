/* compile with "gcc -g -fno-stack-protector mem-trash.c -o mem-trash", run with "./mem-trash" */
#include <stdio.h>
#include <string.h>

#define A_CONSTANT 3.7
const float a_real_language_constant = 4.1;

int do_the_work();              /* prototype */

int main()
{
  int ret = do_the_work();
  return ret;
}

int do_the_work()
{
  char my_string[9];
  int important_array[8];
  int crucial_value;
  int i;

  crucial_value = 42;
  printf("just set crucial_value to: %d\n", crucial_value);
  strcpy(my_string, "this is a string that is longer than what I have allocated for it");
  printf("Just set my_string to be <%s>\n", my_string);
  printf("After setting my_string, crucial_value is: %d\n", crucial_value);
  for (i = 0; i < 8; ++i) {
    important_array[i] = i*i; /* fill this important array with the squares of numbers */
  }
  printf("After setting the array, my_string is <%s>\n", my_string);

  return 0;
}
