#! /usr/bin/env python

import sys
## import gc                       # to explicitly free memory
from time import sleep
import os

def main():
    print('# arguments should be size to use (MB), second is duration (sec)')
    print('# argv: ', sys.argv, len(sys.argv))
    if len(sys.argv) != 3:
        print('  error in arguments: usage is:')
        print('  %s size(MB) time(sec)' % sys.argv[0])
        print('  example: "%s 500 40" will use 500 megabytes for 40 seconds'
              % sys.argv[0])
        sys.exit(1)
    size = int(sys.argv[1])
    duration = int(sys.argv[2])

    ## request big chunk of RAM, then write to each byte
    foo = ('x')*(size*1024*1024)
    #foo = ['abc' for x in range(10**7)]
    print('%d MB allocated; now sleeping for %d seconds' % (size, duration))
    os.system('date --iso=seconds')
    sleep(duration)
    os.system('date --iso=seconds')
    print('DONE; exiting')

if __name__ == '__main__':
    main()
