#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>

/* compile with: */
/* gcc memory-use-c.c -o memory-use-c */

int main(int argc, char *argv[])
{
  if (argc != 3) {
    printf("  error in arguments: usage is:\n");
    printf("  %s size(MB) time(sec)\n", argv[0]);
    printf("  example: \"%s 500 40\" will use 500 megabytes for 40 seconds\n", 
           argv[0]);
    exit(1);
  }
  int size = atoi(argv[1]);
  int duration = atoi(argv[2]);
  /* NOTE: we are not doing any checking on the arguments to see that
     their values are valid numbers for size and duration; a good
     exercise is to improve the program for such checking, possibly
     with assert()
   */
  char *ptr = malloc(size * 1024 * 1024);
  /* NOTE: once we have allocated the memory, we should write
     something to it because a modern memory system might not actually
     grab the RAM until you write to it */
  memset(ptr, 'x', size*1024*1024);

  printf("%d MB allocated; now sleeping for %d seconds\n", size, duration);
  system("date --iso=seconds");
  sleep(duration);
  system("date --iso=seconds");
  printf("DONE; exiting\n");
  return 0;
}
