.. _sec-sharing-on-the-net:

====================
 Sharing on the net
====================

status: barely started - just a placeholder

Motivation and plan
===================

It is important to be able to flexibly put files and documentation in
networked places where you can get to them from different locations.
This also lets you distribute data files, source code, documentation
to other people.

We will discuss a bit of the picture of what's available.  This
changes quite a bit with time, so I will mention that I last
proof-read this introductory section on 2020-05-18.  This will allow
you to see how obsolete this information is.


Static files
============

Content management systems
==========================

Sharing the vieweing of active terminals
========================================

Tools are: screen, tmux, teleconsole, tmate.

:index:`GNU Screen <pair: GNU Screen; screen>`, https://www.gnu.org/software/screen/
   The oldest of these programs, screen was posted to
   comp.sources.unix in 1987 [#f_laumann_email]_.  Originally focused
   on multiplexing a single physical terminal, today it is used more
   for its remarkable ability to stash the session it was in even if
   the connection dies.  You can then pick this session back up from
   another login session coming from a completely different place.

   #. If you are used to using :kbd:`Control-a` (the control key and
      'a' at the same time) to go to the start of a line (and you
      should be used to that) you should bind a different "hot key" in
      your `.screenrc` - I use the backtick character, for example, so
      my `~/.screenrc` file has this line: :code:`escape ``` but in
      the description below I will assume :kbd:`Control-a`.
   #. Type :code:`screen`.
   #. Type a command that puts some text in your screen.
   #. Create a new window with :kbd:`Control-a Control-c` - you will
      not see the original one anymore.
   #. Switch between them with :kbd:`Control-a n`.
   #. Play with creating new ones, switching between them, and closing
      some.
   
   Now for the session detachment:

   #. In your original screen session run a long-running job.  This
      could be a scientific simulation that you do not want
      interrupted when you disconnect, or a long measurement from some
      lab equipment.  Or a trivial shell script like:

      .. code-block:: bash

         for i in `seq 1 1000`
         do
             date --iso=seconds
             sleep 30
         done

   #. Log in to that same host from another terminal window (or from a
      different computer altogether).  If you are curious, keep your
      eye on the original screen: it will be interesting.
   #. Type :code:`screen -Dr` (`-D` will detach the original screen,
      and `-r` will reattach it here.
   #. Learn more keybindings (and how to create your own) with `man
      screen`

   This has all been individual.  Now we'll quit all those screen
   windows and create a new screen to `share with other people!`  This
   is a good way to tutor people and watch what they are doing.

   #. Create a new screen session with just one window and nobody
      attached to it:

      .. code:: bash

         screen -d -m -S one-dude-sharing-with-others

   #. In another terminal list existing sessions and then attach to
      our new session with:

      .. code:: bash

         screen -ls
         screen -x one-dude-sharing-with-others

      and create a second window with :kbd:`Control-a c` and run some
      unique commands in it, like :code:`banner first dude`

   #. In *another* window attach to it in the same way and cycle
      through windows with :kbd:`Control-A n` until you find one that
      does not have the first dude's stuff, and run a unique command
      in that shell, like :code:`banner SECOND dude`

   #. Go back to the first window, cycle through windows until you
      find what the second dude did.

   .. NOTE::

      Sharing that screen with another login name has some extra
      subtleties which are described in the `Network Security Toolkit wiki <https://wiki.networksecuritytoolkit.org/index.php/HowTo_Share_A_Terminal_Session_Using_Screen#Sharing_A_Screen_Session_With_Another_User>`_

:index:`tmux`
   Another terminal multiplexer of more modern vintage than screen.
   tmux offers many of the same features.  It multiplexes a single
   terminal, it allows detaching and reattaching.  The individual
   bindings and commands even look similar to those in screen,
   although the default prefix is :kbd:`Control-b` instead of
   :kbd:`Control-a`.

:index:`teleconsole`
   teleconsole allows you to cast your terminal session to a public
   web page.  This is a good way to give public demonstrations of
   shell procedures and of any programming that can be done in a
   single window.

   Using it is almost too easy: run :code:`teleconsole` and it will
   start a new shell that is shared on the web.  You will be given a
   friendly message with a URL for the session, something that looks
   like this:
   `<https://teleconsole.com/s/c5c10064548622796ef26b189635824f7644dbf6>`_

   You just announce this URL to your "guests" and start typing.
   The only security is the obscurity of that URL.  Also note that
   this program relies on some organization running that
   teleconsole.com web site, otherwise you would have to set up a
   server yourself.

   A lengthier description can be found at
   https://www.2daygeek.com/teleconsole-share-terminal-session-instantly-to-anyone-in-seconds/
   and a very detailed manual is at
   https://gravitational.co/teleport/docs/admin-guide/

:index:`tmate`
   tmate started as a fork of tmux, and it added a rather cool feature
   for sharing with remote users with ssh instead of through the web.
   Right after you start tmate it gives you a line like this one::

      [tmate] ssh session: ssh RCJWHsWqejdwsTaHb69GXzDXG@sfo2.tmate.io

   and you can just tell people to paste that ssh line in to their own
   terminal.  It is it eerily useful to see this mirroring of
   characters and screen info as it gets typed.  Once again this
   relies on someone having set up a public ssh server (in this case
   at tmate.io).



Sharing one's desktop
=====================

The main protocols/tools for this are *vnc* and *rdp*.  There are many
clients and servers for both available on GNU/Linux systems.  I won't
say too much more than pointing out that they exist, since they are
graphical tools and very widely used, so there is not much need to
discuss them here.

Other tools that are worth investigating are *Xpra* and *xmove*.


.. rubric:: Footnotes

.. [#f_laumann_email]
   Posted to usenet by Laumann with the charming headers::

     Submitted-by: Oliver Laumann <seismo!tub!net>
     Newsgroups: comp.sources.unix
     Subject: v10i095:  BSD multi-screen manager, Part01/02
     Date: 7 Aug 87 11:14:56 GMT
