==============================
 Fun and instructive programs
==============================

This is a grabbag of miscellaneous programs I have run in to which are
fun and instructive, but don't merit their own chapter.

Memory profiling with mbw
=========================

I benchmarked memory transfer using the program mbw (available in
debian).  This will give an idea of how fast programs run once they are
fully loaded and don't have much disk I/O.  I ran:

$ mbw 1000

The output shows how long it took and what the transfer rate was.  A
powerful system is one where the transfer rate is large.

On some computers that I had sitting around in 2017 I got:

* magicflute (my main machine):

.. code-block:: console

   markgalassi@magicflute:~$ mbw 1000 | grep AVG
   AVG     Method: MEMCPY  Elapsed: 0.18005        MiB: 1000.00000 Copy: 5553.911 MiB/s
   AVG     Method: DUMB    Elapsed: 0.11505        MiB: 1000.00000 Copy: 8691.858 MiB/s

* papageno (my secondary machine in the other room):

.. code-block:: console

   markgalassi@papageno:~$ mbw 1000 | grep AVG
   AVG     Method: MEMCPY  Elapsed: 0.36072        MiB: 1000.00000 Copy: 2772.267 MiB/s
   AVG     Method: DUMB    Elapsed: 0.35681        MiB: 1000.00000 Copy: 2802.584 MiB/s
   AVG     Method: MCBLOCK Elapsed: 0.26571        MiB: 1000.00000 Copy: 3763.492 MiB/s

* HP stream laptop:

.. code-block:: console

   $ mbw 1000 | grep AVG
   AVG     Method: MEMCPY  Elapsed: 0.36072        MiB: 1000.00000 Copy: 1325.256 MiB/s
   AVG     Method: DUMB    Elapsed: 0.35681        MiB: 1000.00000 Copy: 1429.251 MiB/s
   AVG     Method: MCBLOCK Elapsed: 0.26571        MiB: 1000.00000 Copy: 1967.454 MiB/s

* ASUS C201 chromebook

.. code-block:: console

   $ mbw 1000 | grep AVG
   AVG     Method: MEMCPY  Elapsed: 0.36072        MiB: 1000.00000 Copy: 1129.712 MiB/s
   AVG     Method: DUMB    Elapsed: 0.35681        MiB: 1000.00000 Copy: 1390.579 MiB/s
   AVG     Method: MCBLOCK Elapsed: 0.26571        MiB: 1000.00000 Copy: 1637.766 MiB/s
