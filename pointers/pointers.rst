.. _sec-pointers:

==========
 Pointers
==========

Motivation and plan
===================

Understanding pointers in C is very important:

* Using pointers from a high level language like C allows you to
  understand how your program is being executed in the computer's
  hardware.

* Advanced programming in C involves the use of pointers, and using
  them effectively is important.

* Programming *embedded systems* involves accessing specific memory
  locations which are mapped to hardware functionality.  Writing to
  those memory addresses can configure a peripheral device or transmit
  data, and reading from these memory addresses can receive data or
  give status on the device.

Our plan is to become more familiar with pointers in C and with the
layout of memory on our computers.

You might have access to a Raspberry pi or another device which you
can program to access hardware locations.  A link for this is:

http://www.valvers.com/open-software/raspberry-pi/step01-bare-metal-programming-in-cpt1/

Or you might not have an embedded system available, and that will give
us the opportunity to *simulate* a sample piece of real hardware.

This kind of simulation work is an important type of programming, and
a type that few programmers know how to do.

We will start with a quick review of some basic C programming
techniques that use pointers and create situations where we we copy
chunks of memory.

Then we will have a quick review of 

What are pointers?
==================

Basic pointer code: strings and memory copy
===========================================

strings...

memcpy for big structures

Linked lists
============

self-referential structures

A review of bitwise operators
=============================


Simulating a hardware device
============================

